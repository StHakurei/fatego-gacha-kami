import cv2
import numpy as np
from numpy import ndarray
from abc import ABC, abstractmethod


class FeatureDetectionProvider(ABC):
    @abstractmethod
    def __init__(self, cv2Algorithm: str):
        return

    @abstractmethod
    def getImageDescriptors(self, cv2Image: ndarray):
        return


class ORBFeatureDetectionProvider(FeatureDetectionProvider):
    def __init__(self):
        self.orb = cv2.ORB_create(edgeThreshold=20, fastThreshold=20)

    def getImageDescriptors(self, cv2Image: ndarray):
        return self.orb.detectAndCompute(cv2Image, None)[1]


class FeatureMatchingProvider(ABC):
    @abstractmethod
    def __init__(sefl):
        return

    @abstractmethod
    def getImageMatches(self, fristDescriptors: ndarray, secondDescriptors: ndarray):
        return


class BFFeatureMatchingProvider(FeatureMatchingProvider):
    def __init__(self, matchingMethod=cv2.NORM_HAMMING):
        indexParams = dict(
            algorithm=6, table_number=6, key_size=12, multi_probe_level=1
        )
        searchParams = dict(checks=50)
        self.bf = cv2.FlannBasedMatcher(indexParams, searchParams)

    def getImageMatches(self, fristDescriptors: ndarray, secondDescriptors: ndarray):
        matches = self.bf.knnMatch(fristDescriptors, secondDescriptors, k=2)
        return matches


class TemplateMatchingProvider(ABC):
    @abstractmethod
    def __init__(self, comparisonMethod: str):
        return

    @abstractmethod
    def findObjectFromImage(
        self, baseImage: ndarray, objectImage: ndarray, threshold: int
    ):
        return


class TM_CCOEFF_TemplateMatchingProvider(TemplateMatchingProvider):
    def __init__(self):
        self.method = cv2.TM_CCOEFF

    def findObjectFromImage(
        self, baseImage: ndarray, objectImage: ndarray, threshold=0
    ):
        result = cv2.matchTemplate(baseImage, objectImage, self.method)
        return np.where(result >= threshold)
