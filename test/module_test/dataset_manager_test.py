import numpy as np
from numpy import ndarray
from modules.dataset_manager import DatasetManager
from modules.resource_manager import FGO_DATATYPE
from modules.log_manager import LogManager

FGO_DATASETS = np.array(
    [
        (
            "Servant",
            282,
            "Kijyo Kouyou",
            "Berserker",
            4,
            "https://grandorder.wiki/images/thumb/6/69/Icon_Servant_282.png/70px-Icon_Servant_282.png",
            "avatars/servant_282.png",
            np.array([[254, 89, 117], [27, 237, 238]], dtype=np.uint8),
        )
    ],
    dtype=FGO_DATATYPE,
)

FGO_DATASETS_WITHOUT_DESCRIPTORS = np.array(
    [
        (
            "Servant",
            282,
            "Kijyo Kouyou",
            "Berserker",
            4,
            "https://grandorder.wiki/images/thumb/6/69/Icon_Servant_282.png/70px-Icon_Servant_282.png",
            "avatars/servant_282.png",
            np.array([]),
        )
    ],
    dtype=FGO_DATATYPE,
)

FGO_DATASETS_WITH_WRONG_LOCALFILE = np.array(
    [
        (
            "Servant",
            282,
            "Kijyo Kouyou",
            "Berserker",
            4,
            "https://grandorder.wiki/images/thumb/6/69/Icon_Servant_282.png/70px-Icon_Servant_282.png",
            "avatars/servant_282.png",
            np.array([]),
        )
    ],
    dtype=FGO_DATATYPE,
)

INVALID_ARRAY = [1, 2, 3]
INVALID_NUMPY_ARRAY = np.array(["a", "b", "c"])


log = LogManager()
datasetManager = DatasetManager(log)


def testDatasetManagerWithInvalidAlgorithm():
    try:
        object = DatasetManager(log, "Wrong")
        assert False
    except:
        assert True


def testUpdateDatasetsByOnlineResource():
    try:
        datasetManager.updateDatasetsByOnlineResource(FGO_DATASETS)
        assert True
    except:
        assert False


def testUpdateDatasetsByOnlineResourceWithWrongLocalPath():
    try:
        datasetManager.updateDatasetsByOnlineResource(FGO_DATASETS)
        assert True
    except:
        assert False


def testUpdateDatasetsByOnlineResourceWithInvalidArray():
    try:
        datasetManager.updateDatasetsByOnlineResource(INVALID_ARRAY)
        assert False
    except:
        assert True


def testUpdateDatasetsByOnlineResourceWithInvalidNumPyArray():
    try:
        datasetManager.updateDatasetsByOnlineResource(INVALID_NUMPY_ARRAY)
        assert False
    except:
        assert True


def testDatasetsToLocalFile():
    try:
        datasetManager.saveDatasetsToLocalFile(FGO_DATASETS, "fatego_test.npy")
        assert True
    except:
        assert False


def testDatasetsToLocalFileWithInvalidArray():
    try:
        datasetManager.saveDatasetsToLocalFile(INVALID_ARRAY, "fatego_test.npy")
        assert False
    except:
        assert True


def testDatasetsToLocalFileInvalidNumpyArray():
    try:
        datasetManager.saveDatasetsToLocalFile(INVALID_NUMPY_ARRAY, "fatego_test.npy")
        assert False
    except:
        assert True


def testLoadDatasetsFromLocalFile():
    try:
        results = datasetManager.loadDatasetsFromLocalFile("fatego_test.npy")
        assert type(results) == ndarray and results.dtype == FGO_DATATYPE
    except:
        assert False


def testLoadDatasetsFromLocalFileWithWrongFile():
    try:
        results = datasetManager.loadDatasetsFromLocalFile("nothing.npy")
        assert False
    except:
        assert True
