from modules.gacha_result_analyser import GachaResultAnalyser
from modules.gacha_screenshot_operator import GachaScreenshotOperator
from modules.dataset_manager import DatasetManager
from modules.gacha_record_manager import FGO_RECORD_DATATYPE
from modules.log_manager import LogManager
from interfaces.opencv import *
import numpy as np

TEST_SCREENSHOT_FILE = "test/analysis_target.jpg"
SAMPLE_RESULTS = np.array(
    [
        (
            1,
            "Servant",
            "Hassan of the Serenity",
            "Assassin",
            3,
            "PyTest Banner",
            "2022-09-05 23:57:50",
        ),
        (
            2,
            "Craft Essence",
            "Extremely Spicy Mapo Tofu",
            "None",
            3,
            "PyTest Banner",
            "2022-09-05 23:57:50",
        ),
        (
            3,
            "Craft Essence",
            "Phantasmal Species",
            "None",
            3,
            "PyTest Banner",
            "2022-09-05 23:57:50",
        ),
        (
            4,
            "Craft Essence",
            "Noble Piece",
            "None",
            3,
            "PyTest Banner",
            "2022-09-05 23:57:51",
        ),
        (
            5,
            "Craft Essence",
            "Elixir of Love",
            "None",
            3,
            "PyTest Banner",
            "2022-09-05 23:57:51",
        ),
        (
            6,
            "Craft Essence",
            "The Moment of Peace",
            "None",
            4,
            "PyTest Banner",
            "2022-09-05 23:57:51",
        ),
        (
            7,
            "Craft Essence",
            "Battle of Camlann",
            "None",
            3,
            "PyTest Banner",
            "2022-09-05 23:57:51",
        ),
        (
            8,
            "Servant",
            "Kiyohime",
            "Berserker",
            3,
            "PyTest Banner",
            "2022-09-05 23:57:51",
        ),
        (
            9,
            "Craft Essence",
            "A Pilgrimage To The Other Side",
            "None",
            5,
            "PyTest Banner",
            "2022-09-05 23:57:51",
        ),
        (
            10,
            "Craft Essence",
            "Imaginary Number Magecraft",
            "None",
            4,
            "PyTest Banner",
            "2022-09-05 23:57:51",
        ),
        (
            11,
            "Craft Essence",
            "Let's Depart!",
            "None",
            3,
            "PyTest Banner",
            "2022-09-05 23:57:51",
        ),
    ],
    dtype=FGO_RECORD_DATATYPE,
)

log = LogManager()
orb = ORBFeatureDetectionProvider()
datasetManager = DatasetManager(log)
gachaScreenshotOperator = GachaScreenshotOperator(
    log,
    orb,
    TM_CCOEFF_TemplateMatchingProvider(),
)
gachaResultAnalyser = GachaResultAnalyser(log, orb, BFFeatureMatchingProvider())

datasets = datasetManager.loadDatasetsFromLocalFile()


def testSetDatasets():
    try:
        gachaResultAnalyser.setDatasets(datasets)
        assert True
    except:
        assert False


def testSetBannerName():
    try:
        gachaResultAnalyser.setBannerName("PyTest Banner")
        assert True
    except:
        assert False


def testBatchRecognizeGachaResults():
    try:
        results = gachaResultAnalyser.batchRecognizeGachaResults(
            gachaScreenshotOperator.extraGachaResultsFromScreenshot(
                TEST_SCREENSHOT_FILE
            )
        )
        for sampleResut, recognizedResult in zip(SAMPLE_RESULTS, results):
            if sampleResut["Roll"] != recognizedResult["Roll"]:
                assert False
            if sampleResut["Type"] != recognizedResult["Type"]:
                assert False
            if sampleResut["Name"] != recognizedResult["Name"]:
                assert False
            if sampleResut["Class"] != recognizedResult["Class"]:
                assert False
            if sampleResut["Rarity"] != recognizedResult["Rarity"]:
                assert False
            if sampleResut["Banner"] != recognizedResult["Banner"]:
                assert False

        assert True
    except:
        assert False
