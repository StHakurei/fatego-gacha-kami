from modules.gacha_screenshot_operator import (
    GachaScreenshotOperator,
    STD_SCREENSHOT_HEIGHT,
    STD_SCREENSHOT_WIDTH,
)
from interfaces.opencv import *
from modules.log_manager import LogManager

VALID_10ROLL_SCREENSHOT = "test/10RollScreenshot.jpg"
VALID_11ROLL_SCREENSHOT = "test/11RollScreenshot.jpg"
VALID_IPAD_SCREENSHOT = "test/ipadScreenshot.jpeg"
INVALID_GACHA_SCREEMSHOT = "test/invalid_screenshot.jpg"
NOT_AN_IMAGE_FILE = "good_history.csv"

CONSISTENT_SCREENSHOTS_DIR = "test/consistent_screenshots"
UNCONSISTENT_SCREENSHOTS_DIR = "test/unconsistent_screenshots"

gachaScreenshotOperator = GachaScreenshotOperator(
    LogManager(),
    ORBFeatureDetectionProvider(),
    TM_CCOEFF_TemplateMatchingProvider(),
)


def testStandardizeGachaScreenshot():
    try:
        resizedScreenshot = gachaScreenshotOperator.standardizeGachaScreenshot(
            VALID_10ROLL_SCREENSHOT
        )
        assert resizedScreenshot.shape[0] == STD_SCREENSHOT_HEIGHT
    except:
        assert False


def testStandardizeGachaScreenshotWithiPadScreenshot():
    try:
        resizedScreenshot = gachaScreenshotOperator.standardizeGachaScreenshot(
            VALID_IPAD_SCREENSHOT
        )
        assert resizedScreenshot.shape[1] == STD_SCREENSHOT_WIDTH
    except:
        assert False


def testStandardizeGachaScreenshotWithNotImageFile():
    try:
        resizedScreenshot = gachaScreenshotOperator.standardizeGachaScreenshot(
            NOT_AN_IMAGE_FILE
        )
        assert False
    except:
        assert True


def testIs10RollScressnhot():
    try:
        resizedScreenshot = gachaScreenshotOperator.standardizeGachaScreenshot(
            VALID_10ROLL_SCREENSHOT
        )
        assert gachaScreenshotOperator.is10RollScreenshot(
            resizedScreenshot, VALID_10ROLL_SCREENSHOT
        )
    except:
        assert False


def testIs10RollScressnhotWithInvalidScreenshot():
    try:
        resizedScreenshot = gachaScreenshotOperator.standardizeGachaScreenshot(
            INVALID_GACHA_SCREEMSHOT
        )
        assert not gachaScreenshotOperator.is10RollScreenshot(
            resizedScreenshot, INVALID_GACHA_SCREEMSHOT
        )
    except:
        assert False


def testIs11RollScressnhot():
    try:
        resizedScreenshot = gachaScreenshotOperator.standardizeGachaScreenshot(
            VALID_11ROLL_SCREENSHOT
        )
        assert gachaScreenshotOperator.is11RollScreenshot(
            resizedScreenshot, VALID_11ROLL_SCREENSHOT
        )
    except:
        assert False


def testIs11RollScressnhotWithInvalidScreenshot():
    try:
        resizedScreenshot = gachaScreenshotOperator.standardizeGachaScreenshot(
            INVALID_GACHA_SCREEMSHOT
        )
        assert not gachaScreenshotOperator.is11RollScreenshot(
            resizedScreenshot, INVALID_GACHA_SCREEMSHOT
        )
    except:
        assert False


def testExtraGachaResultsFromScreenshotWith10RollScreenshot():
    try:
        assert (
            len(
                gachaScreenshotOperator.extraGachaResultsFromScreenshot(
                    VALID_10ROLL_SCREENSHOT
                )
            )
            == 10
        )
    except:
        assert False


def testExtraGachaResultsFromScreenshotWith11RollScreenshot():
    try:
        assert (
            len(
                gachaScreenshotOperator.extraGachaResultsFromScreenshot(
                    VALID_11ROLL_SCREENSHOT
                )
            )
            == 11
        )
    except:
        assert False


def testIsConsistentScreenshots():
    try:
        assert gachaScreenshotOperator.isConsistentScreenshots(
            CONSISTENT_SCREENSHOTS_DIR
        )
    except:
        assert False


def testIsConsistentScreenshotsWithUnconsistentScreenshots():
    try:
        assert not gachaScreenshotOperator.isConsistentScreenshots(
            UNCONSISTENT_SCREENSHOTS_DIR
        )
    except:
        assert False
