from modules.resource_manager import OnlineResourceManager
from modules.log_manager import LogManager
import os

VALID_DOWNLOAD_URL = "https://static.wikia.nocookie.net/fategrandorder/images/5/59/CEIcon1.webp/revision/latest/scale-to-width-down/75?cb=20230325095950"
INVALID_DOWNLOAD_URL = (
    "htstps://static.wikia.nocookie.nscale-to-width-down/75?cb=20220427131350"
)

VALID_LOCAL_PATH = "test/avatar.png"
INVALID_LOCA_PATH = "wrong/avatar.png"

onlineResourceManager = OnlineResourceManager(LogManager())


def testDownloadImage():
    try:
        onlineResourceManager.downloadImage(VALID_DOWNLOAD_URL, VALID_LOCAL_PATH)
        os.remove(VALID_LOCAL_PATH)
        assert True
    except:
        assert False


def testDownloadImageWithInvalidPath():
    try:
        onlineResourceManager.downloadImage(VALID_DOWNLOAD_URL, INVALID_LOCA_PATH)
        assert False
    except:
        assert True


def testDownloadImageWithInvalidURL():
    try:
        onlineResourceManager.downloadImage(INVALID_DOWNLOAD_URL, VALID_LOCAL_PATH)
        assert False
    except:
        assert True


def testGetServantsData():
    try:
        assert len(onlineResourceManager.getServantsData()) > 0
    except:
        assert False


def testGetCraftEssencesData():
    try:
        assert len(onlineResourceManager.getCraftEssencesData()) > 0
    except:
        assert False
