import modules.utils as utils
import numpy as np
import os

# Prepate test data.
TITLE = "PyTest Table"
COLUNMS = np.array(["Roll", "Name", "Type", "Class", "Rarity", "Banner", "Time"])
WRONG_COLUNMS = np.array(
    ["Roll", "Name", "Type", "Class", "Rarity", "Banner", "Time", "Other"]
)
ROWS = np.array(
    [
        [1, "Hakurei", "Servant", "Caster", "5", "Test Banner", "2077-01-01 20:23:34"],
        [
            2,
            "Hakurei Reime",
            "Servant",
            "Caster",
            4,
            "Test Banner",
            "2077-01-01 20:23:34",
        ],
        [
            3,
            "Hakurei Reime",
            "Servant",
            "Caster",
            4,
            "Test Banner",
            "2077-01-01 20:23:34",
        ],
        [
            4,
            "Hakurei Reime",
            "Servant",
            "Caster",
            4,
            "Test Banner 1233123",
            "2077-01-01 20:23:34",
        ],
        [
            5,
            "Hakurei Reime",
            "Servant",
            "Caster",
            4,
            "Test Banner",
            "2077-01-01 20:23:34",
        ],
        [
            6,
            "Hakurei Reime",
            "Servant",
            "Caster",
            4,
            "Test Banner",
            "2077-01-01 20:23:34",
        ],
        [
            7,
            "Hakurei Reime",
            "Servant",
            "Caster",
            4,
            "Test Banner",
            "2077-01-01 20:23:34",
        ],
        [
            8,
            "Hakurei Reime",
            "Servant",
            "Caster",
            4,
            "Test Banner",
            "2077-01-01 20:23:34",
        ],
        [
            9,
            "Hakurei Reime",
            "Servant",
            "Caster",
            4,
            "Test Banner",
            "2077-01-01 20:23:34",
        ],
        [
            10,
            "Templum",
            "Craft Essence",
            "None",
            4,
            "Test Banner",
            "2077-01-01 20:23:34",
        ],
    ]
)
WRONG_ROWS = np.array(
    [
        [1, "Hakurei", "Servant", "Caster", "5", "Test Banner", "2077-01-01 20:23:34"],
        [2, "Hakurei Reime", "Servant", "Caster", "2077-01-01 20:23:34"],
        [
            3,
            "Hakurei Reime",
            "Servant",
            "Caster",
            4,
            "Test Banner",
            "2077-01-01 20:23:34",
        ],
        [
            4,
            "Hakurei Reime",
            "Servant",
            "Caster",
            4,
            "Test Banner 1233123",
            "2077-01-01 20:23:34",
        ],
        [
            5,
            "Hakurei Reime",
            "Servant",
            "Caster",
            4,
            "Test Banner",
            "2077-01-01 20:23:34",
        ],
        [
            6,
            "Hakurei Reime",
            "Servant",
            "Caster",
            4,
            "Test Banner",
            "2077-01-01 20:23:34",
        ],
        [
            7,
            "Hakurei Reime",
            "Servant",
            "Caster",
            4,
            "Test Banner",
            "2077-01-01 20:23:34",
        ],
        [
            8,
            "Hakurei Reime",
            "Servant",
            "Caster",
            4,
            "Test Banner",
            "2077-01-01 20:23:34",
        ],
        [9, "Hakurei Reime", 4, "Test Banner", "2077-01-01 20:23:34"],
        [
            10,
            "Templum",
            "Craft Essence",
            "None",
            4,
            "Test Banner",
            "2077-01-01 20:23:34",
        ],
    ],
    dtype=object,
)

VALID_LOCAL_PATH = "test/11RollScreenshot.jpg"
INVALID_LOCA_PATH = "tests/avatar.png"


def testGetTimestamp():
    try:
        result = utils.getTimestamp()
        assert True
    except:
        assert False


def testGetDatestamp():
    try:
        result = utils.getDatestamp()
        assert True
    except:
        assert False


def testFileExists():
    if utils.fileExists(VALID_LOCAL_PATH):
        assert True
    else:
        assert False


def testCreateDir():
    try:
        utils.createDir("NewDirectory")
        os.rmdir("NewDirectory")
        assert True
    except:
        assert False


def testFileExistsWithInvalidPath():
    if not utils.fileExists(INVALID_LOCA_PATH):
        assert True
    else:
        assert False


def testPrintInfoTable():
    try:
        result = utils.printInfoTable(TITLE, COLUNMS, ROWS)
        assert True
    except:
        assert False


def testPrintInfoTableWithWrongColumns():
    try:
        result = utils.printInfoTable(TITLE, WRONG_COLUNMS, ROWS)
        assert False
    except:
        assert True


def testPrintInfoTableWithWrongRows():
    try:
        result = utils.printInfoTable(TITLE, COLUNMS, WRONG_ROWS)
        assert False
    except:
        assert True
