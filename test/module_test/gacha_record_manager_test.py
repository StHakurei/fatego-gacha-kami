import numpy as np
from modules.log_manager import LogManager
from modules.gacha_record_manager import GachaRecordManager, FGO_RECORD_DATATYPE

NORMAL_HISTORY_FILE = "test/good_history.csv"
WRONG_HISTORY_FILE = "test/wrong_history.csv"
INVALID_HEARDER_HISTORY_FILE = "test/invalid_header_history.csv"
INVALID_RECORD_HISTORY_FILE = "test/invalid_record_history.csv"
SAVED_HISTORY_FILE = "test/saved_history.csv"

SINGLE_GACHA_RECORD = np.array(
    (
        31,
        "Craft Essence",
        "Fuuma Kotarou",
        "Assassin",
        3,
        "Thanksgiving 2020 Pickup Summon",
        "2021-05-19 20:20:59",
    ),
    dtype=FGO_RECORD_DATATYPE,
)
WRONG_GACHA_RECORD = [
    100,
    "Servant",
    "Someone",
    "Saber",
    4,
    "New Banner",
    "2021-05-19 20:20:59",
]

gachaRecordManager = GachaRecordManager(LogManager(), verbose=False)


def testloadGachaHistoryFromFile():
    try:
        gachaRecordManager.loadGachaHistoryFromFile(NORMAL_HISTORY_FILE)
        assert gachaRecordManager.getGachaHistoryRecords().size == 30
    except:
        assert False


def testloadGachaHistoryFromFileWithWrongFile():
    try:
        gachaRecordManager.loadGachaHistoryFromFile(WRONG_HISTORY_FILE)
        assert False
    except:
        assert True


def testVerifyGachaHistory():
    try:
        gachaRecordManager.verifyGachaHistory(NORMAL_HISTORY_FILE)
        assert len(gachaRecordManager.getGachaHistoryErrorDetails()) == 0
    except:
        assert False


def testVerifyGachaHistoryWithWrongFile():
    try:
        gachaRecordManager.verifyGachaHistory(WRONG_HISTORY_FILE)
        assert False
    except:
        assert True


def testVerifyGachaHistoryWithInvalidHeader():
    try:
        gachaRecordManager.verifyGachaHistory(INVALID_HEARDER_HISTORY_FILE)
        assert len(gachaRecordManager.getGachaHistoryErrorDetails()) == 2
    except:
        assert False


def testVerifyGachaHistoryWithInvalidRecords():
    try:
        gachaRecordManager.verifyGachaHistory(INVALID_RECORD_HISTORY_FILE)
        assert len(gachaRecordManager.getGachaHistoryErrorDetails()) == 7
    except:
        assert False


def testAppendGachaHistory():
    try:
        gachaRecordManager.loadGachaHistoryFromFile(NORMAL_HISTORY_FILE)
        gachaRecordManager.appendGachaHistoryRecords(SINGLE_GACHA_RECORD)
        assert gachaRecordManager.getGachaHistoryRecords().size == 31
    except:
        assert False


def testSaveGachaHistoryToFile():
    try:
        gachaRecordManager.saveGachaHistoryToFile(SAVED_HISTORY_FILE)
        assert True
    except:
        assert False
