from services.datasets_service import FGODatasetsService


def testUpdateFGODatasets():
    try:
        FGODatasetsService.updateFGODatasets(verbose=False)
        assert True
    except:
        assert False


def testRefreshFGODatasets():
    try:
        FGODatasetsService.refreshFGODatasets(verbose=False)
        assert True
    except:
        assert False
