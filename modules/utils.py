import os
import datetime
from numpy import ndarray
from .application_exception import *


def getTimestamp():
    return str(datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S"))


def getDatestamp():
    return str(datetime.datetime.now().strftime("%Y%m%d"))


def fileExists(filePath: str):
    return os.path.exists(filePath)


def createDir(dirName: str):
    try:
        os.makedirs(dirName)
    except Exception as error:
        print(f"Failed to create directory '{dirName}'. \n{error}")
        exit(0)


def getCPUCount():
    return os.cpu_count()


def printInfoTable(tableTitle: str, tableColunms: ndarray, tableRows: ndarray):
    if (type(tableTitle), type(tableColunms), type(tableRows)) != (
        str,
        ndarray,
        ndarray,
    ):
        raise DataTypeError(
            "printInfoTable(tableTitle: str, tableColunms: ndarray, tableRows: ndarray)"
        )

    tableColunmCount = len(tableColunms)
    maxContentLengths = [(lambda value: len(value))(colunm) for colunm in tableColunms]

    for rowIndex in range(0, len(tableRows)):
        if tableColunmCount != len(tableRows[rowIndex]):
            raise MismatchedTableColunmWithContentFieldError()

        for index, value in enumerate(tableRows[rowIndex]):
            if maxContentLengths[index] < len(str(value)):
                maxContentLengths[index] = len(str(value))

    maxTableLength = sum(maxContentLengths) + len(maxContentLengths) * 3 + 1

    def printTableLine(length: int, lineSymbol: str):
        for i in range(0, length):
            print(lineSymbol, end="")
        print()

    def printTableRow(contents: ndarray, contentLengths: list):
        tableRow = ""
        for length in contentLengths:
            tableRow = tableRow + "| {:^" + str(length) + "} "
        tableRow = tableRow + "|"
        print(tableRow.format(*contents))

    print(f"{tableTitle}")
    printTableLine(maxTableLength, "=")
    printTableRow(tableColunms, maxContentLengths)
    printTableLine(maxTableLength, "-")
    for rowIndex in range(0, len(tableRows)):
        printTableRow(tableRows[rowIndex], maxContentLengths)
    printTableLine(maxTableLength, "-")
