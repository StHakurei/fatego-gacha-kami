class ApplicationError(Exception):
    pass


class DataTypeError(ApplicationError):
    def __init__(self, message="Wrong type data was provided."):
        self.message = message
        super().__init__(self.message)


class MismatchedTableColunmWithContentFieldError(ApplicationError):
    def __init__(
        self, message="The given table colunms is mismatched with the content fields."
    ):
        self.message = message
        super().__init__(self.message)


class UnsupportedCV2FeatureDetectionAlgorithm(ApplicationError):
    def __init__(self, cv2Algorithm: str):
        self.message = f"Algorithm {cv2Algorithm} is not supported."
        super().__init__(self.message)


class UnsupportedCV2FeatureMatchingAlgorithm(ApplicationError):
    def __init__(self, cv2Algorithm: str):
        self.message = f"Algorithm {cv2Algorithm} is not supported."
        super().__init__(self.message)


class UnsupportedCV2TemplateDetectionAlgorithm(ApplicationError):
    def __init__(self, cv2Algorithm: str):
        self.message = f"Algorithm {cv2Algorithm} is not supported."
        super().__init__(self.message)


class GachaResultExtractionError(ApplicationError):
    def __init__(self, screenshotType: str, screenshotPath: str):
        self.message = (
            f"Failed to extra {screenshotType} results from {screenshotPath}."
        )
        super().__init__(self.message)


class UnknownGachaScreenshotType(ApplicationError):
    def __init__(self, screenshotPath: str):
        super().__init__(f"Failed to recognize gacha screenshot. -> {screenshotPath}")


class MismatchedGachaResults(ApplicationError):
    def __init__(
        self, extractedResultCount: int, screenshotType: int, screenshotPath: str
    ):
        super().__init__(
            f"Extracted gacha results count {extractedResultCount} is mismatched with screenshot type {screenshotType}. -> {screenshotPath}"
        )


class InconsistentGachaScreenshots(ApplicationError):
    def __init__(self, screenshotDir):
        super().__init__(f"Detect inconsistent gacha screenshots in {screenshotDir}")


class UnknownServantClass(ApplicationError):
    def __init__(self, message="Cannot identify servant class."):
        self.message = message
        super().__init__(self.message)
