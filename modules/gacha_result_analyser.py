from modules.utils import *
from modules.application_exception import *
from modules.log_manager import LogManager
from modules.gacha_record_manager import FGO_RECORD_DATATYPE
from config.servants import NOT_AVAILABLE_SERVANT_IDS
from config.craftEssence import NOT_AVAILABLE_CRAFTESSENCE_IDS
from interfaces.opencv import FeatureDetectionProvider, FeatureMatchingProvider
from numpy import ndarray
import numpy as np


class GachaResultAnalyser(object):
    def __init__(
        self,
        log: LogManager,
        featureDetection: FeatureDetectionProvider,
        featureMatching: FeatureMatchingProvider,
    ):
        self.log = log
        self.featureDetection = featureDetection
        self.featureMatching = featureMatching

    def __isAvailableServant(self, servantId: int):
        return servantId not in NOT_AVAILABLE_SERVANT_IDS

    def __isAvailableCraftEssence(self, craftEssenceId: int):
        return craftEssenceId not in NOT_AVAILABLE_CRAFTESSENCE_IDS

    def __findGoodMatches(self, matches: tuple):
        goodMatches = []
        for matche in matches:
            if len(matche) == 2:
                m, n = matche
                if m.distance < 0.7 * n.distance:
                    goodMatches.append([m])
        return goodMatches

    def __recognizeSingleResult(
        self,
        gachaResult: ndarray,
        resultSlotIndex: int,
        recognizedGachaResults: ndarray,
    ):
        bestMatcheScore = 0
        bestMatcheIndex = -1
        for index, dataset in enumerate(self.datasets):
            matches = self.featureMatching.getImageMatches(
                gachaResult, dataset["Descriptors"]
            )
            goodMatches = self.__findGoodMatches(matches)

            matcheScore = len(goodMatches) / len(matches)
            if matcheScore > bestMatcheScore:
                bestMatcheScore = matcheScore
                bestMatcheIndex = index

        recognizedGachaResults[resultSlotIndex] = (
            resultSlotIndex + 1,
            self.datasets[bestMatcheIndex]["Type"],
            self.datasets[bestMatcheIndex]["Name"],
            self.datasets[bestMatcheIndex]["Class"],
            self.datasets[bestMatcheIndex]["Rarity"],
            self.bannerName,
            getTimestamp(),
        )
        self.log.addInfoLog(
            f'Recognize gacha result {resultSlotIndex} -> {self.datasets[bestMatcheIndex]["Type"]}:{self.datasets[bestMatcheIndex]["Name"]} - Score:{bestMatcheScore} - Datasets index: {bestMatcheIndex} - Avatar: {self.datasets[bestMatcheIndex]["LocalPath"]}'
        )

    def setDatasets(self, datasets: ndarray):
        availableDatasetIndexe = []
        for index in range(0, datasets.size):
            if datasets[index]["Type"] == "Servant" and not self.__isAvailableServant(
                datasets[index]["Id"]
            ):
                continue

            if datasets[index][
                "Type"
            ] == "Craft Essence" and not self.__isAvailableCraftEssence(
                datasets[index]["Id"]
            ):
                continue

            availableDatasetIndexe.append(index)

        self.datasets = datasets[availableDatasetIndexe]
        self.log.addInfoLog(f"Filtered available datasets {self.datasets.size}")

    def setBannerName(self, bannerName: str):
        self.bannerName = bannerName
        self.log.addInfoLog(f"Set FGO banner name: {bannerName}")

    def batchRecognizeGachaResults(self, gachaResults: list):
        if type(gachaResults) is not list:
            raise DataTypeError(
                "Python list is required.\n-> GachaResultAnalyser::batchRecognizeGachaResults(self, gachaResults: list)"
            )

        for index, result in enumerate(gachaResults):
            if type(result) is not ndarray:
                raise DataTypeError(
                    f"numpy.ndarray is required at list element {index}.\n-> GachaResultAnalyser::batchRecognizeGachaResults(self, gachaResults: list)"
                )

        self.log.addInfoLog(f"Input gacha results {len(gachaResults)}")
        recognizedGachaResults = np.empty(len(gachaResults), dtype=FGO_RECORD_DATATYPE)

        for index in range(0, len(gachaResults)):
            self.__recognizeSingleResult(
                self.featureDetection.getImageDescriptors(gachaResults[index]),
                index,
                recognizedGachaResults,
            )

        return recognizedGachaResults
