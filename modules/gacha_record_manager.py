import csv
import time
import traceback
import concurrent.futures
import numpy as np
from numpy import ndarray
from modules.log_manager import LogManager
from modules.utils import *
from modules.application_exception import *

GACHA_HISTORY_FILE = "user_data/history.csv"
GACHA_HISTORY_HEADER = [
    "Roll",
    "Type",
    "Name",
    "Class",
    "Rarity",
    "Banner",
    "Record Time",
]
GACHA_DATA_INDEX_MAP = {
    "Roll": 0,
    "Type": 1,
    "Name": 2,
    "Class": 3,
    "Rarity": 4,
    "Banner": 5,
    "Record Time": 6,
}
SERVANT_CLASSES = [
    "Saber",
    "Lancer",
    "Archer",
    "Rider",
    "Caster",
    "Assassin",
    "Berserker",
    "Shielder",
    "Ruler",
    "Avenger",
    "Mooncancer",
    "Alterego",
    "Foreigner",
    "Pretender"
]
CARD_RARITY = ["1", "2", "3", "4", "5"]
MAX_CARD_NAME_LENGTH = 100
MAX_BANNER_NAME_LENGTH = 200
FGO_RECORD_DATATYPE = np.dtype(
    {
        "names": ["Roll", "Type", "Name", "Class", "Rarity", "Banner", "Record Time"],
        "formats": ["u2", "U13", "U100", "U20", "u1", "U200", "U19"],
    }
)


class GachaRecordManager(object):
    def __init__(self, log: LogManager, verbose=True):
        self.log = log
        self.encode = "utf-8"
        self.verbose = verbose
        self.gachaHistoryErrorDetails = []
        self.gachaHistoryRecords = None

    def __verifyRollCount(self, rawContent: list, index: int):
        if rawContent[GACHA_DATA_INDEX_MAP.get("Roll")] != str(index - 1):
            errorMessage = f"Roll count is discontinuous at line {index}"
            self.log.addErrorLog(errorMessage)
            self.gachaHistoryErrorDetails.append(errorMessage)

    def __verifyResultType(self, rawContent: list, index: int):
        if rawContent[GACHA_DATA_INDEX_MAP.get("Type")] not in [
            "Servant",
            "Craft Essence",
        ]:
            errorMessage = (
                f"Type value should be 'Servant' or 'Craft Essence' at line {index}"
            )
            self.log.addErrorLog(errorMessage)
            self.gachaHistoryErrorDetails.append(errorMessage)

    def __verifyCardNameLength(self, rawContent: list, index: int):
        if len(rawContent[GACHA_DATA_INDEX_MAP.get("Name")]) > MAX_CARD_NAME_LENGTH:
            errorMessage = f"The length of Name value exceeded {MAX_CARD_NAME_LENGTH} at line {index}"
            self.log.addErrorLog(errorMessage)
            self.gachaHistoryErrorDetails.append(errorMessage)

    def __verifyClassName(self, rawContent: list, index: int):
        if rawContent[GACHA_DATA_INDEX_MAP.get("Type")] == "Craft Essence":
            if rawContent[GACHA_DATA_INDEX_MAP.get("Class")] != "None":
                errorMessage = f"When Type value is Craft Essence, the Class value should be 'None' at line {index}"
                self.log.addErrorLog(errorMessage)
                self.gachaHistoryErrorDetails.append(errorMessage)

        if rawContent[GACHA_DATA_INDEX_MAP.get("Type")] == "Servant":
            if rawContent[GACHA_DATA_INDEX_MAP.get("Class")] not in SERVANT_CLASSES:
                errorMessage = f"When Type is Servant, the Class value should be in {SERVANT_CLASSES} at line {index}"
                self.log.addErrorLog(errorMessage)
                self.gachaHistoryErrorDetails.append(errorMessage)

    def __verifyRarity(self, rawContent: list, index: int):
        if rawContent[GACHA_DATA_INDEX_MAP.get("Rarity")] not in CARD_RARITY:
            errorMessage = f"Rarity value should be in {CARD_RARITY} at line {index}"
            self.log.addErrorLog(errorMessage)
            self.gachaHistoryErrorDetails.append(errorMessage)

    def __verifyBannerNameLength(self, rawContent: list, index: int):
        if len(rawContent[GACHA_DATA_INDEX_MAP.get("Banner")]) > MAX_BANNER_NAME_LENGTH:
            errorMessage = f"The length of Banner value exceeded {MAX_BANNER_NAME_LENGTH} at line {index}"
            self.log.addErrorLog(errorMessage)
            self.gachaHistoryErrorDetails.append(errorMessage)

    def __verifyTimestamp(self, rawContent: list, index: int):
        try:
            time.strptime(
                rawContent[GACHA_DATA_INDEX_MAP.get("Record Time")],
                "%Y-%m-%d %H:%M:%S",
            )
        except:
            errorMessage = (
                f"Record Time's format should be YYYY-MM-DD HH:MM:ss at line {index}"
            )
            self.log.addErrorLog(errorMessage)
            self.gachaHistoryErrorDetails.append(errorMessage)

    def __verifyGachaRecordRow(self, rawContent: list, index: int):
        if len(rawContent) != 7:
            errorMessage = f"Gacha record contains {len(rawContent)} elements (expected 7) at line {index}"
            self.log.addErrorLog(errorMessage)
            self.gachaHistoryErrorDetails.append(errorMessage)
        else:
            if rawContent != GACHA_HISTORY_HEADER:
                self.__verifyRollCount(rawContent, index)
                self.__verifyResultType(rawContent, index)
                self.__verifyCardNameLength(rawContent, index)
                self.__verifyClassName(rawContent, index)
                self.__verifyRarity(rawContent, index)
                self.__verifyBannerNameLength(rawContent, index)
                self.__verifyTimestamp(rawContent, index)

    def __verifyGachaRecordHeader(self, rawContent: list, index: int):
        if rawContent[index] != GACHA_HISTORY_HEADER:
            checkMessage = f"Gacha history data header should be {GACHA_HISTORY_HEADER}"
            self.log.addErrorLog(checkMessage)
            self.gachaHistoryErrorDetails.append(checkMessage)

    def verifyGachaHistory(self, filePath=GACHA_HISTORY_FILE):
        try:
            self.log.addInfoLog(f"Verify gacha history file {filePath}", self.verbose)
            rawContent = []
            with open(filePath, newline="", encoding="utf-8") as csvFile:
                reader = csv.reader(csvFile, delimiter=",", quotechar="|")
                for row in reader:
                    rawContent.append(row)

            self.__verifyGachaRecordHeader(rawContent, 0)

            with concurrent.futures.ThreadPoolExecutor(
                max_workers=getCPUCount()
            ) as executor:
                executor.map(
                    self.__verifyGachaRecordRow, rawContent, range(1, len(rawContent))
                )
        except Exception:
            self.log.addErrorLog(
                f"Failed to verify gacha history file: {filePath}",
                traceback.format_exc(),
                self.verbose,
            )

    def getGachaHistoryErrorDetails(self):
        return self.gachaHistoryErrorDetails

    def loadGachaHistoryFromFile(self, filePath=GACHA_HISTORY_FILE):
        try:
            self.gachaHistoryRecords = np.genfromtxt(
                filePath,
                skip_header=1,
                delimiter=",",
                dtype=FGO_RECORD_DATATYPE,
                encoding=self.encode,
            )
            self.log.addInfoLog(
                f"Loaded {self.gachaHistoryRecords.size} gacha history records from {filePath}.",
                self.verbose,
            )
        except Exception:
            self.log.addErrorLog(
                f"Unable to load gacha history from {filePath}",
                traceback.format_exc(),
                self.verbose,
            )

    def getGachaHistoryRecords(self):
        return self.gachaHistoryRecords

    def appendGachaHistoryRecords(self, gachaRecords: ndarray):
        if type(gachaRecords) is not ndarray:
            raise DataTypeError(
                "numpy.ndarray is required.\n-> GachaRecordManager::appendGachaHistoryRecords(self, gachaRecords: ndarray)"
            )

        if gachaRecords.dtype != FGO_RECORD_DATATYPE:
            raise DataTypeError(
                "numpy.dtype FGO_RECORD_DATATYPE is required.\n-> GachaRecordManager::appendGachaHistoryRecords(self, gachaRecords: ndarray)"
            )

        self.gachaHistoryRecords = np.hstack((self.gachaHistoryRecords, gachaRecords))
        self.log.addInfoLog(
            f"Add {gachaRecords.size} records to gacha history.", self.verbose
        )

    def saveGachaHistoryToFile(self, filePath=GACHA_HISTORY_FILE):
        try:
            np.savetxt(
                filePath,
                self.gachaHistoryRecords,
                fmt="%s",
                newline="\n",
                header=",".join(GACHA_HISTORY_HEADER),
                delimiter=",",
                comments="",
                encoding="utf-8",
            )
            self.log.addInfoLog(
                f"Saved {self.gachaHistoryRecords.size} gacha history records to {filePath}.",
                self.verbose,
            )
        except Exception:
            self.log.addErrorLog(
                f"Unable to save gacha history to {filePath}",
                traceback.format_exc(),
                self.verbose,
            )
