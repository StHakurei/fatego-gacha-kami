from .utils import *


class LogManager(object):
    def __init__(self):
        self.encode = "utf-8"
        self.logFilePath = f"fatego-gacha-kami-{getDatestamp()}.log"
        if not fileExists(self.logFilePath):
            try:
                open(self.logFilePath, "w", encoding=self.encode).close()
            except OSError as ex:
                print("Warning: Unable to create running log file [Permission Denied].")
                print(ex)
                exit(0)
            else:
                self.addInfoLog("Created a new log file.")

    def getLogFilePath(self):
        return self.logFilePath

    def addInfoLog(self, message: str, verbose=False):
        content = f"{getTimestamp()} | INFO | {message}"
        if verbose:
            print(content)
        with open(self.logFilePath, "a", encoding=self.encode) as log:
            log.write(content + "\n")

    def addErrorLog(self, message: str, trace=None, verbose=False):
        content = f"{getTimestamp()} | ERROR | {message}"
        if verbose:
            print(content)
        with open(self.logFilePath, "a", encoding=self.encode) as log:
            log.writelines(content + "\n")
            if trace:
                log.write(f"{trace}\n")
