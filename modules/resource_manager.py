import requests
import numpy as np
import traceback
from bs4 import BeautifulSoup
from bs4.element import Tag
from requests.adapters import HTTPAdapter
from modules.log_manager import LogManager
from modules.application_exception import DataTypeError, UnknownServantClass
from modules.utils import *


FGO_SERVANT_URL = "https://fategrandorder.fandom.com/wiki/Servant_List"
FGO_ESSENCE_URLS = [
    "https://fategrandorder.fandom.com/wiki/Craft_Essence_List/By_ID/1-100",
    "https://fategrandorder.fandom.com/wiki/Craft_Essence_List/By_ID/101-200",
    "https://fategrandorder.fandom.com/wiki/Craft_Essence_List/By_ID/201-300",
    "https://fategrandorder.fandom.com/wiki/Craft_Essence_List/By_ID/301-400",
    "https://fategrandorder.fandom.com/wiki/Craft_Essence_List/By_ID/401-500",
    "https://fategrandorder.fandom.com/wiki/Craft_Essence_List/By_ID/501-600",
    "https://fategrandorder.fandom.com/wiki/Craft_Essence_List/By_ID/601-700",
    "https://fategrandorder.fandom.com/wiki/Craft_Essence_List/By_ID/701-800",
    "https://fategrandorder.fandom.com/wiki/Craft_Essence_List/By_ID/801-900",
    "https://fategrandorder.fandom.com/wiki/Craft_Essence_List/By_ID/901-1000",
    "https://fategrandorder.fandom.com/wiki/Craft_Essence_List/By_ID/1001-1100",
    "https://fategrandorder.fandom.com/wiki/Craft_Essence_List/By_ID/1101-1200",
    "https://fategrandorder.fandom.com/wiki/Craft_Essence_List/By_ID/1201-1300",
    "https://fategrandorder.fandom.com/wiki/Craft_Essence_List/By_ID/1301-1400",
    "https://fategrandorder.fandom.com/wiki/Craft_Essence_List/By_ID/1401-1500",
    "https://fategrandorder.fandom.com/wiki/Craft_Essence_List/By_ID/1501-1600",
    "https://fategrandorder.fandom.com/wiki/Craft_Essence_List/By_ID/1601-1700",
    "https://fategrandorder.fandom.com/wiki/Craft_Essence_List/By_ID/1701-1800",
    "https://fategrandorder.fandom.com/wiki/Craft_Essence_List/By_ID/1801-1900",
    "https://fategrandorder.fandom.com/wiki/Craft_Essence_List/By_ID/1901-2000",
]
HTTP_MAX_RETRY = 5
FGO_DATATYPE = np.dtype(
    {
        "names": [
            "Type",
            "Id",
            "Name",
            "Class",
            "Rarity",
            "AvatarURL",
            "LocalPath",
            "Descriptors",
        ],
        "formats": ["U13", "u2", "U50", "U20", "u1", "U200", "U50", ndarray],
    }
)


class OnlineResourceManager(object):
    def __init__(self, log: LogManager):
        self.log = log
        self.request = requests.Session()
        self.request.mount("https://", HTTPAdapter(max_retries=HTTP_MAX_RETRY))

    def __getHTMLContent(self, url: str):
        try:
            return self.request.get(url)
        except Exception as error:
            self.log.addErrorLog(
                f"Unable to get HTML content.\nURL:{url}",
                error,
            )
            print(f"Unable to get HTML content.\nURL:{url}\n{error}")
            exit(0)

    def __findServantClass(self, wikiContent: str):
        if "Saber" in wikiContent:
            return "Saber"
        elif "Archer" in wikiContent:
            return "Archer"
        elif "Lancer" in wikiContent:
            return "Lancer"
        elif "Caster" in wikiContent:
            return "Caster"
        elif "Assassin" in wikiContent:
            return "Assassin"
        elif "Rider" in wikiContent:
            return "Rider"
        elif "Berserker" in wikiContent:
            return "Berserker"
        elif "Moon Cancer" in wikiContent:
            return "Mooncancer"
        elif "Foreigner" in wikiContent:
            return "Foreigner"
        elif "Alter Ego" in wikiContent:
            return "Alterego"
        elif "Ruler" in wikiContent:
            return "Ruler"
        elif "Avenger" in wikiContent:
            return "Avenger"
        elif "Shielder" in wikiContent:
            return "Shielder"
        elif "Beast" in wikiContent:
            return "Beast"
        elif "Pretender" in wikiContent:
            return "Pretender"
        else:
            return "Unknown"

    def __extraServantsData(self, tableContent: Tag):
        if type(tableContent) is not Tag:
            raise DataTypeError("bs4.element.Tag is required.")

        className = tableContent.h2.span.get_text()

        if className == "Unknown":
            raise UnknownServantClass()

        servantsTable = tableContent.find_all("tr")
        servantsData = np.zeros(len(servantsTable) - 1, dtype=FGO_DATATYPE)

        for rowIndex in range(1, len(servantsTable)):
            servant = servantsTable[rowIndex].find_all("td")

            if len(servant) != 0:
                try:
                    servantId = int(servant[3].get_text())
                except:
                    servantId = 0  # For servants who are missing ID on fandom.com.
                try:
                    avatarUrl = servant[0].find("img")["data-src"]
                except:
                    avatarUrl = servant[0].find("img")["src"]

                servantsData[rowIndex - 1] = (
                    "Servant",
                    servantId,
                    servant[1].find("a")["title"],
                    self.__findServantClass(className),
                    servant[2].get_text().count("★"),
                    avatarUrl.replace("75?cb=", "70?cb="),
                    f"avatars/servant_{servantId}.png",
                    np.array([]),
                )

        return servantsData

    def downloadImage(self, url: str, savePath: str):
        try:
            image = self.request.get(url, stream=True)
            if image.status_code == 200:
                with open(savePath, "wb") as file:
                    for chunk in image.iter_content(chunk_size=1024):
                        if chunk:
                            file.write(chunk)
                    self.log.addInfoLog(f"Download image {url} -> {savePath}")
            else:
                self.log.addErrorLog(
                    f"Unable to download resouce from {url} - {image.status_code} {image.reason}"
                )
                print(
                    f"Unable to download resouce from {url} - {image.status_code} {image.reason}"
                )
                exit(0)

        except Exception as error:
            self.log.addErrorLog(
                f"Unable to download and save image.\nURL:{url}\nPath:{savePath}", error
            )
            print(f"Unable to download and save image.\nURL:{url}\nPath:{savePath}")
            exit(0)

    def getServantsData(self):
        html = self.__getHTMLContent(FGO_SERVANT_URL)
        if html.status_code != 200:
            self.log.addErrorLog(
                f"Failed to get fandom.com content. {html.status_code} - {html.reason}"
            )
            print(
                f"Failed to get fandom.com content. {html.status_code} - {html.reason}"
            )
            exit(0)

        try:
            allClassServantTable = (
                BeautifulSoup(html.content, "html.parser")
                .find("div", class_="tabber wds-tabber")
                .find_all("div", class_="wds-tab__content")
            )

            allServantData = np.zeros(0, dtype=FGO_DATATYPE)
            for tableIndex in range(1, len(allClassServantTable)):
                allServantData = np.hstack(
                    (
                        allServantData,
                        self.__extraServantsData(allClassServantTable[tableIndex]),
                    )
                )

            return allServantData
        except Exception:
            self.log.addErrorLog(
                "Failed to parse fandom.com content.",
                traceback.format_exc(),
            )
            print(
                f"Failed to parse fandom.com content.\n{traceback.format_exc()}"
            )
            exit(0)

    def getCraftEssencesData(self):
        craftEssencesTable = []
        for rowIndex in range(0, len(FGO_ESSENCE_URLS)):
            html = self.__getHTMLContent(FGO_ESSENCE_URLS[rowIndex])
            if html.status_code != 200:
                self.log.addErrorLog(
                    f"Failed to get fandom.com content. {html.status_code} - {html.reason}"
                )
                print(
                    f"Failed to get fandom.com content. {html.status_code} - {html.reason}"
                )
                exit(0)
            craftEssencesTable.extend(
                BeautifulSoup(html.content, "html.parser")
                .find("table", class_="sortable")
                .find_all("tr")
            )

        craftEssencesData = np.zeros(
            len(craftEssencesTable) - len(FGO_ESSENCE_URLS), dtype=FGO_DATATYPE
        )
        dataIndex = 0

        try:
            for tableIndex in range(1, len(craftEssencesTable)):
                craftEssence = craftEssencesTable[tableIndex].find_all("td")
                if len(craftEssence) != 0:
                    craftEssenceId = int(craftEssence[3].get_text())
                    try:
                        avatarUrl = craftEssence[0].find("img")["data-src"]
                    except:
                        avatarUrl = craftEssence[0].find("img")["src"]

                    craftEssencesData[dataIndex] = (
                        "Craft Essence",
                        craftEssenceId,
                        craftEssence[1].get_text().strip(),
                        "None",
                        craftEssence[2].get_text().count("★"),
                        avatarUrl.replace("75?cb=", "70?cb="),
                        f"avatars/craftessence_{craftEssenceId}.png",
                        np.array([]),
                    )
                    dataIndex = dataIndex + 1
            return craftEssencesData
        except Exception:
            self.log.addErrorLog(
                "Failed to parse fategrandorder.fandom online content.",
                traceback.format_exc(),
            )
            print(
                f"Failed to parse fategrandorder.fandom online content.\n{traceback.format_exc()}"
            )
            exit(0)
