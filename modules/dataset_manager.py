import numpy as np
import concurrent.futures
import traceback
from numpy import ndarray
from modules.resource_manager import FGO_DATATYPE
from modules.utils import *
from modules.application_exception import *
from modules.log_manager import LogManager
from interfaces.opencv import *
import cv2

FGO_DATASET_FILE = "datasets/fatego_en.npy"


class DatasetManager(object):
    def __init__(self, log: LogManager, cv2Algorithm="ORB"):
        self.log = log
        if cv2Algorithm == "ORB":
            self.featureDetectionProvider: FeatureDetectionProvider = (
                ORBFeatureDetectionProvider()
            )
        else:
            self.log.addErrorLog(
                f"Unsupported feature detection algorithm {cv2Algorithm} was provided."
            )
            raise UnsupportCV2FeatureDetectionAlgorithm(cv2Algorithm)
        self.log.addInfoLog(
            f"Initialize OpenCV feature detection algorithm {cv2Algorithm}."
        )

    def __updateAvatarDescriptor(self, onlineResource: ndarray, index: int):
        avatarFile = onlineResource["LocalPath"]
        if fileExists(avatarFile):
            try:
                onlineResource[
                    "Descriptors"
                ] = self.featureDetectionProvider.getImageDescriptors(
                    cv2.imread(avatarFile)
                )
                self.log.addInfoLog(f"Update avatar descriptors for {avatarFile}")
            except Exception:
                self.log.addErrorLog(
                    f"Failed to get image descriptors for {avatarFile}",
                    trace=traceback.format_exc(),
                )
        else:
            self.log.addErrorLog(f"Avatar file not found. Path: {avatarFile}")

    def updateDatasetsByOnlineResource(self, onlineResources: ndarray):
        if type(onlineResources) is not ndarray:
            raise DataTypeError("numpy.ndarray is required.")

        if onlineResources.dtype != FGO_DATATYPE:
            raise DataTypeError("numpy.dtype FGO_DATATYPE is required.")

        with concurrent.futures.ThreadPoolExecutor(
            max_workers=getCPUCount()
        ) as executor:
            executor.map(
                self.__updateAvatarDescriptor,
                onlineResources,
                range(len(onlineResources)),
            )

        return onlineResources

    def loadDatasetsFromLocalFile(self, filePath=FGO_DATASET_FILE):
        try:
            self.log.addInfoLog(f"Read datasets from local file: {filePath}")
            return np.load(filePath, allow_pickle=True)
        except Exception:
            self.log.addErrorLog(
                f"Failed to load datasets from local file: {filePath}",
                trace=traceback.format_exc(),
            )
            print(
                f"Failed to load datasets from local file: {filePath}\n{traceback.format_exc()}"
            )
            exit(0)

    def saveDatasetsToLocalFile(
        self, onlineResources: ndarray, filePath=FGO_DATASET_FILE
    ):
        if type(onlineResources) is not ndarray:
            raise DataTypeError("numpy.ndarray is required.")

        if onlineResources.dtype != FGO_DATATYPE:
            raise DataTypeError("numpy.dtype FGO_DATATYPE is required.")

        try:
            np.save(filePath, onlineResources)
            self.log.addInfoLog(f"Saved datasets to local file: {filePath}")
        except Exception:
            self.log.addErrorLog(
                f"Failed to save datasets to local file: {filePath}",
                trace=traceback.format_exc(),
            )
            print(
                f"Failed to save datasets to local file: {filePath}\n{traceback.format_exc()}"
            )
            exit(0)
