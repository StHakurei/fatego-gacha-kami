from modules.gacha_record_manager import FGO_RECORD_DATATYPE, GACHA_HISTORY_HEADER
from modules.log_manager import LogManager
from modules.utils import *
import traceback
import json
import time
import numpy as np


class GachaReportManager:
    def __init__(self, log: LogManager):
        self.log = log

    def loadGachaHistory(self, gachaHistory: ndarray):
        if type(gachaHistory) is not ndarray:
            raise DataTypeError(
                "numpy.ndarray is required.\n-> GachaRecordManager::appendGachaHistoryRecords(self, gachaRecords: ndarray)"
            )

        if gachaHistory.dtype != FGO_RECORD_DATATYPE:
            raise DataTypeError(
                "numpy.dtype FGO_RECORD_DATATYPE is required.\n-> GachaRecordManager::appendGachaHistoryRecords(self, gachaRecords: ndarray)"
            )
        self.gachaHistory = gachaHistory
        self.log.addInfoLog("Suceessfully read gacha history into RAM.")

    def printBannerSummaryReport(self, bannerName: str):
        rolls = 0
        servants = {}
        essences = {}
        try:
            for record in self.gachaHistory:
                if record["Banner"] == bannerName:
                    if record["Type"] == "Servant":
                        if record["Name"] in servants:
                            servants[record["Name"]] = servants[record["Name"]] + 1
                        else:
                            servants[record["Name"]] = 1

                    if record["Type"] == "Craft Essence":
                        if record["Name"] in essences:
                            essences[record["Name"]] = essences[record["Name"]] + 1
                        else:
                            essences[record["Name"]] = 1
                    rolls = rolls + 1

            print(
                json.dumps(
                    {
                        "event": bannerName,
                        "iconId": "N/A",
                        "pack": 0,
                        "saintQuartz": 0,
                        "ticket": 0,
                        "summoning": rolls,
                        "servant": servants,
                        "craftEssences": essences,
                    },
                    ensure_ascii=False,
                    indent=4,
                )
            )
            self.log.addInfoLog(
                f"Suceessfully generated banner summary report for banner {bannerName}"
            )
        except Exception:
            self.log.addErrorLog(
                f"Failed to print banner summary report for banner {bannerName}",
                trace=traceback.format_exc(),
                verbose=True,
            )

    def printBannerRecords(self, bannerName: str):
        try:
            self.log.addInfoLog(
                f"Suceessfully generated banner summary report for banner {bannerName}"
            )
            startTime = time.time()
            selectedRecords = []
            for record in self.gachaHistory:
                if record["Banner"] == bannerName:
                    selectedRecords.append(record)

            if len(selectedRecords) != 0:
                printInfoTable(
                    tableTitle=f'Selected {len(selectedRecords)} Records for "{bannerName}"',
                    tableColunms=np.array(GACHA_HISTORY_HEADER),
                    tableRows=np.array(selectedRecords, dtype=FGO_RECORD_DATATYPE),
                )
                print("Finished in %.4f seconds" % (time.time() - startTime))
            else:
                print("Nothing found in your history")
        except Exception:
            self.log.addErrorLog(
                f"Failed to print banner summary report for banner {bannerName}",
                trace=traceback.format_exc(),
                verbose=True,
            )
