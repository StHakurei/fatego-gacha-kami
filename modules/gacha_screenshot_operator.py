from modules.log_manager import LogManager
from config.colors import BGRColor
from interfaces.opencv import (
    FeatureDetectionProvider,
    TemplateMatchingProvider,
)
from numpy import ndarray
from modules.application_exception import *
import cv2
import traceback
import os

# In pixel.
STD_SCREENSHOT_HEIGHT = 288
STD_SCREENSHOT_WIDTH = 512
SELECT_BOX_HEIGHT = 76
SELECT_BOX_WIDTH = 70
SELECT_BOX_OFFSET = 75
FIRST_RESULT_X_OFFSET_11ROLLS = 225
FIRST_RESULT_Y_OFFSET_11ROLLS = 185
FIRST_RESULT_X_OFFSET_10ROLLS = 226
FIRST_RESULT_Y_OFFSET_10ROLLS = 185
SEVENTH_RESULT_X_OFFSET_11ROLLS = 188
SEVENTH_RESULT_Y_OFFSET_11ROLLS = 95
SEVENTH_RESULT_X_OFFSET_10ROLLS = 151
SEVENTH_RESULT_Y_OFFSET_10ROLLS = 95

ANKER_IMAGE_10ROLLS_PATH = "config/10RollsAnker.png"
ANKER_IMAGE_11ROLLS_PATH = "config/11RollsAnker.png"

TEMPLATE_MATCHING_THRESHOLD = 15000000


class GachaScreenshotOperator(object):
    def __init__(
        self,
        log: LogManager,
        featureDetection: FeatureDetectionProvider,
        templateMatching: TemplateMatchingProvider,
        verbose=False,
    ):
        self.log = log
        self.featureDetection = featureDetection
        self.templateMatching = templateMatching
        self.x10Anker = cv2.imread(ANKER_IMAGE_10ROLLS_PATH)
        self.x11Anker = cv2.imread(ANKER_IMAGE_11ROLLS_PATH)
        self.verbose = verbose

    def __getSelectBoxBottomRightLoc(
        self,
        topLeftLoc: tuple,
    ):
        return (
            topLeftLoc[0] + SELECT_BOX_WIDTH,
            topLeftLoc[1] + SELECT_BOX_HEIGHT,
        )

    def __getAnkerBottomRightLoc(self, ankerLoc: tuple, ankerImage: ndarray):
        return (
            ankerLoc[0] + ankerImage.shape[1],
            ankerLoc[1] + ankerImage.shape[0],
        )

    def __moveSelectBoxToRight(self, resultLoc: tuple, movePixel: int):
        return (resultLoc[0] + movePixel, resultLoc[1])

    def __extraResultsByFirstResultLoc(self, firstResultLoc: tuple, resultCount: int):
        resultLocs = []
        for index in range(0, resultCount):
            resultLocs.append(
                self.__moveSelectBoxToRight(firstResultLoc, SELECT_BOX_OFFSET * index)
            )
        return resultLocs

    def __getFirstResultLocByAnkerLoc(self, ankerLoc: tuple, screenshotType: int):
        if screenshotType == 10:
            return (
                ankerLoc[0] - FIRST_RESULT_X_OFFSET_10ROLLS,
                ankerLoc[1] - FIRST_RESULT_Y_OFFSET_10ROLLS,
            )

        if screenshotType == 11:
            return (
                ankerLoc[0] - FIRST_RESULT_X_OFFSET_11ROLLS,
                ankerLoc[1] - FIRST_RESULT_Y_OFFSET_11ROLLS,
            )

    def __getSeventhResultLocByAnkerLoc(self, ankerLoc: tuple, screenshotType: int):
        if screenshotType == 10:
            return (
                ankerLoc[0] - SEVENTH_RESULT_X_OFFSET_10ROLLS,
                ankerLoc[1] - SEVENTH_RESULT_Y_OFFSET_10ROLLS,
            )

        if screenshotType == 11:
            return (
                ankerLoc[0] - SEVENTH_RESULT_X_OFFSET_11ROLLS,
                ankerLoc[1] - SEVENTH_RESULT_Y_OFFSET_11ROLLS,
            )

    def __getGachaResultLocs(self, ankerLoc: tuple, screenshotType: int, debug=False):
        resultLocs = []
        resultLoc1 = self.__getFirstResultLocByAnkerLoc(ankerLoc, screenshotType)
        resultLoc7 = self.__getSeventhResultLocByAnkerLoc(ankerLoc, screenshotType)
        if debug:
            print(f"Result 1 location = {resultLoc1}")
            print(f"Result 7 location = {resultLoc7}")
        resultLocs.extend(self.__extraResultsByFirstResultLoc(resultLoc1, 6))
        if screenshotType == 10:
            resultLocs.extend(self.__extraResultsByFirstResultLoc(resultLoc7, 4))
        if screenshotType == 11:
            resultLocs.extend(self.__extraResultsByFirstResultLoc(resultLoc7, 5))
        return resultLocs

    def standardizeGachaScreenshot(self, imagePath: str):
        try:
            screenshot = cv2.imread(imagePath)
            height, width = screenshot.shape[0:2]
            if (height / width) >= 0.75:
                ratio = STD_SCREENSHOT_WIDTH / width
            else:
                ratio = STD_SCREENSHOT_HEIGHT / height
            height = int(screenshot.shape[0] * ratio)
            width = int(screenshot.shape[1] * ratio)
            dim = (width, height)
            standardizedScreenshot = cv2.resize(
                screenshot, dim, interpolation=cv2.INTER_AREA
            )
            self.log.addInfoLog(
                f"Resized image {imagePath} to {height}:h {width}:w.",
                verbose=self.verbose,
            )
            return standardizedScreenshot
        except:
            self.log.addErrorLog(
                f"Unable to resize image {imagePath}.",
                traceback.format_exc(),
                verbose=True,
            )
            exit(0)

    def is10RollScreenshot(self, screenshotImage: ndarray, screenshotPath: str):
        try:
            ankerLocs = self.templateMatching.findObjectFromImage(
                screenshotImage, self.x10Anker, TEMPLATE_MATCHING_THRESHOLD
            )
            return len(list(zip(*ankerLocs))) == 1
        except:
            self.log.addErrorLog(
                f"Failed to run OpenCV template matching on file {screenshotPath}.",
                traceback.format_exc(),
                verbose=True,
            )
            exit(0)

    def is11RollScreenshot(self, screenshotImage: ndarray, screenshotPath: str):
        try:
            ankerLocs = self.templateMatching.findObjectFromImage(
                screenshotImage, self.x11Anker, TEMPLATE_MATCHING_THRESHOLD
            )
            return len(list(zip(*ankerLocs))) == 1
        except:
            self.log.addErrorLog(
                f"Failed to run OpenCV template matching on file {screenshotPath}.",
                traceback.format_exc(),
                verbose=True,
            )
            exit(0)

    def isConsistentScreenshots(self, screenshotDir: str):
        x10ScreenshotCount = 0
        x11ScreenshotCount = 0
        for root, dirs, files in os.walk(screenshotDir):
            for filename in files:
                if filename != "PutYourScreenshotsInHere":
                    if self.is10RollScreenshot(
                        self.standardizeGachaScreenshot(f"{root}/{filename}"),
                        f"{root}/{filename}",
                    ):
                        x10ScreenshotCount = x10ScreenshotCount + 1

                    if self.is11RollScreenshot(
                        self.standardizeGachaScreenshot(f"{root}/{filename}"),
                        f"{root}/{filename}",
                    ):
                        x11ScreenshotCount = x11ScreenshotCount + 1

        return not (x10ScreenshotCount != 0 and x11ScreenshotCount != 0)

    def extraGachaResultsFromScreenshot(self, screenshotPath: str, debug=False):
        try:
            stdScreenshot = self.standardizeGachaScreenshot(screenshotPath)
            screenshotType = None
            if self.is10RollScreenshot(stdScreenshot, screenshotPath):
                self.log.addInfoLog(
                    f"{screenshotPath} is x10 FateGO gacha screenshot.",
                    verbose=self.verbose,
                )
                screenshotType = 10
                ankerLoc = list(
                    zip(
                        *self.templateMatching.findObjectFromImage(
                            stdScreenshot, self.x10Anker, TEMPLATE_MATCHING_THRESHOLD
                        )[::-1]
                    )
                )[0]
                ankerBottomRight = self.__getAnkerBottomRightLoc(
                    ankerLoc, self.x10Anker
                )
                resultLocs = self.__getGachaResultLocs(ankerLoc, screenshotType)

            if self.is11RollScreenshot(stdScreenshot, screenshotPath):
                self.log.addInfoLog(
                    f"{screenshotPath} is x11 FateGO gacha screenshot.",
                    verbose=self.verbose,
                )
                screenshotType = 11
                ankerLoc = list(
                    zip(
                        *self.templateMatching.findObjectFromImage(
                            stdScreenshot, self.x11Anker, TEMPLATE_MATCHING_THRESHOLD
                        )[::-1]
                    )
                )[0]
                ankerBottomRight = self.__getAnkerBottomRightLoc(
                    ankerLoc, self.x11Anker
                )
                resultLocs = self.__getGachaResultLocs(ankerLoc, screenshotType)

            if not screenshotType:
                self.log.addErrorLog(
                    f"Unable to identify gacha screenshot type on file -> {screenshotPath}",
                    verbose=True,
                )
                raise UnknownGachaScreenshotType(screenshotPath)

            if debug:
                print(f"Anker location = {ankerLoc}")
                cv2.rectangle(
                    stdScreenshot, ankerLoc, ankerBottomRight, BGRColor.GREEN.value, 2
                )

                for index in range(0, len(resultLocs)):
                    cv2.rectangle(
                        stdScreenshot,
                        resultLocs[index],
                        self.__getSelectBoxBottomRightLoc(resultLocs[index]),
                        BGRColor.YELLOW.value,
                        2,
                    )

                cv2.imshow(f"Selected {screenshotType} Results", stdScreenshot)
                cv2.waitKey()
                cv2.destroyAllWindows()

            gachaResults = []
            for index in range(0, len(resultLocs)):
                gachaResults.append(
                    stdScreenshot[
                        resultLocs[index][1] : resultLocs[index][1] + SELECT_BOX_HEIGHT,
                        resultLocs[index][0] : resultLocs[index][0] + SELECT_BOX_WIDTH,
                    ].copy()
                )

            if len(gachaResults) != screenshotType:
                self.log.addErrorLog(
                    f"Extracted gacha results {len(gachaResults)} is mismatched with screenshot type x{screenshotType}.",
                    verbose=True,
                )
                raise MismatchedGachaResults(
                    len(gachaResults), screenshotType, screenshotPath
                )

            return gachaResults
        except:
            self.log.addErrorLog(
                f"Unable to extra gacha results from image {screenshotPath}.",
                traceback.format_exc(),
                verbose=True,
            )
            exit(0)
