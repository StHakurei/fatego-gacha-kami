<div align="center">
    <img src="doc/FateGO_logo.png"/>
</div>

[![pipeline status](https://gitlab.com/StHakurei/fatego-gacha-kami/badges/master/pipeline.svg)](https://gitlab.com/StHakurei/fatego-gacha-kami/pipelines)
[![coverage report](https://gitlab.com/StHakurei/fatego-gacha-kami/badges/master/coverage.svg)](https://gitlab.com/StHakurei/fatego-gacha-kami/-/commits/master)

[[_TOC_]]


# FateGO Gacha Kami

FateGO Gacha Kami is a gacha assistance tool, it could help you to identify servants and craft essences from gacha result screenshots, and save the results to a local csv file. This Python program is powered by [OpenCV](https://pypi.org/project/opencv-python/) and [NumPy](https://numpy.org/).

<div align="center">
    <img src="doc/opencv.png"/>
    <img src="doc/numpy.png"/>
</div>

FateGO Gacha Kami supports both 10 rolls and 11 rolls screenshots, if you want to track and save your gacha history, this program will provide an easy way to do that, all you need to do is take a screenshot of your gacha result, and leave the screenshot to the program!

<div align="center">
    <img src="doc/readme4.png"/>
</div>

<div align="center">
    <img src="doc/readme1.png"/>
</div>

<div align="center">
    <img src="doc/readme2.png"/>
</div>

<div align="center">
    <img src="doc/readme3.png"/>
</div>

## Game region support

Currently FateGO Gacha Kami supports US region, the servant data is from [GrandOrder Wiki](https://grandorder.wiki/), the craft essences data is from [Fandom Wikia](https://fategrandorder.fandom.com/wiki/Fate/Grand_Order_Wikia).

## Installation

**For Ubuntu.**
```
$ sudo apt install python3.10 git
$ git clone https://gitlab.com/StHakurei/fatego-gacha-kami.git
$ cd fatego-gacha-kami
$ pip3 -r requirements.txt
```

## Usage

**Put your gacha result screenshots into '*screenshots*' folder, to avoid duplicate records, please kindly remember that delete or move the screenshots out of the folder.**

1. Get usage information.

   `$ python3 main.py --help` or `$ python3 main.py -h`

2. Check your local gacha history file. This command could help you to verify the format of your history csv file.

   `$ python3 main.py --check-history`

3. Start gacha screenshots identification.
   ```
   $ python3 main.py --recognize --banner "BANNER_NAME"
   $ python3 main.py -r --banner "BANNER_NAME"

   # And add --verbose or -v for detail log.
   $ python3 main.py -r --banner "BANNER_NAME" --verbose
   ```

4. Update datasets from online resources. This command will try to read latest servants and craft essence from fandom and wiki, therefore internet is required.

   `$ python3 main.py --update-datasets`

5. Update avatar image descriptors which are used in gacha result recognition. This operation doesn't require network access.

   `$ python3 main.py --refresh-datasets`

6. Display your gacha history in a SQL-like format.

   `$ python3 main.py --select --banner 'BANNER_NAME'`

7. Generate banner summary report.

   `$ python3 main.py --banner-report --banner 'BANNER_NAME'`


## User Data

All your recognized gacha results will be saved to a local csv file in `./user_data/history.csv`, please feel free to backup it to another location. You can directly modify the csv file for adding records manually if needed, but remember to run `python3 main.py --check-history` to ensure the format is correct.

## Log

All execution log will be redirect to local log file in the repository folder, such as `fatego-gacha-kami-YYYYMMDD.log`, it's very useful in troubleshooting, if you are running into some issues, please attach your log file when opening the issue on GitLab. 

## About Performance

The recognition time is depend on your CPU single core performance, in average, detect and identify 100 gacha results will take 9 seconds.
Baseline CPU: AMD Ryzen 3700X

## Troubleshooting

### What if I use ticket in summon?
Please modify csv file directly.

### Template update process was disrupted by network issue.
Just re-run the program, it will check which dataset or template need to update and skip which are already updated.

### Program won't recognize some gacha results.
First, please confirm your screenshot is come from US game region, currently Fatego Gacha Kami doesn't support JP, TW or other region which the game progress is lead to the US region. If your screenshot is come from US region, please open an issue and attach log file to me, thanks.

## License
[![MIT](https://img.shields.io/badge/license-MIT-green)](LICENSE)
