"""
Program entry and Command Line Tool.
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Provides user interactive console.
"""

__author__ = "Langston.Hakurei"
__version__ = "2.0"
__status__ = "Production"

import argparse
from services.datasets_service import FGODatasetsService
from services.recognition_service import FGOGachaRecognitionService
from services.gacha_record_service import GachaRecordService
from services.gacha_report_service import GachaReportService

params = argparse.ArgumentParser(description=f"FateGO Gacha Kami - v{__version__}")

params.add_argument(
    "--banner",
    type=str,
    default="default_banner",
    help="Assign a banner name for recognize.",
)
params.add_argument(
    "--banner-report",
    action="store_true",
    help="Generate banner report. Set specific banner by adding --banner 'Banner Name'",
)
params.add_argument(
    "--select",
    action="store_true",
    help="Output all gacha records of a banner. Use this option with --banner 'Banner Name'",
)
params.add_argument(
    "-r",
    "--recognize",
    action="store_true",
    help="Recognize gacha results from FGO gacha screenshots.",
)
params.add_argument(
    "--update-datasets", action="store_true", help="Update program datasets."
)
params.add_argument(
    "--refresh-datasets",
    action="store_true",
    help="Refresh the avatar descriptors from current datasets.",
)
params.add_argument(
    "--check-history", action="store_true", help="Check your local gacha history file."
)
params.add_argument(
    "-v", "--verbose", action="store_true", help="Show running details."
)
params.add_argument(
    "--debug", action="store_true", help="Display screenshot selections."
)
params.add_argument(
    "-V", "--version", action="store_true", help="Show program version."
)

args = params.parse_args()

version_info = f"""FateGO Gacha Kami: A gacha assistance tool. Version: {__version__}.
Repository & Document: https://gitlab.com/StHakurei/fatego-gacha-kami"""

if __name__ == "__main__":
    try:
        if args.version:
            print(version_info)

        if args.update_datasets:
            FGODatasetsService.updateFGODatasets(args.verbose)

        if args.refresh_datasets:
            FGODatasetsService.refreshFGODatasets(args.verbose)

        if args.check_history:
            GachaRecordService.checkGachaHistoryFile()

        if args.recognize:
            FGOGachaRecognitionService.recognizeGachaScreenshots(
                verbose=args.verbose, debug=args.debug, bannerName=args.banner
            )

        if args.banner_report:
            GachaReportService.printBannerSummaryReport(args.banner)

        if args.select:
            GachaReportService.printBannerReport(args.banner)

    except KeyboardInterrupt:
        print("\nKeyboard interrupted, quiting...")
        exit(0)
