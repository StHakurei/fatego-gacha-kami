from modules.resource_manager import OnlineResourceManager
from modules.dataset_manager import DatasetManager
from modules.log_manager import LogManager
from modules.utils import *
from numpy import ndarray
import numpy as np


class FGODatasetsService:
    def __maintainAvatarDirectory():
        if not (fileExists("avatars")):
            createDir("avatars")

    def __maintainDatasetDirectory():
        if not (fileExists("datasets")):
            createDir("datasets")

    def __getInvalidDescriptorsCount(datasets: ndarray):
        count = 0
        for index in range(1, len(datasets)):
            if datasets[index]["Descriptors"].size == 0:
                count = count + 1
        return count

    @staticmethod
    def updateFGODatasets(verbose=True):
        log = LogManager()
        log.addInfoLog("Start FGODatasetsService::updateFGODatasets")
        if verbose:
            from progress.bar import ShadyBar
        FGODatasetsService.__maintainAvatarDirectory()
        FGODatasetsService.__maintainDatasetDirectory()
        onlineResource = OnlineResourceManager(log)
        datasetManager = DatasetManager(log)

        fgoDatasets = np.hstack(
            (onlineResource.getServantsData(), onlineResource.getCraftEssencesData())
        )
        log.addInfoLog(
            f"Update {fgoDatasets.size} records from online resource.", verbose
        )
        log.addInfoLog("Start to download avatar images.", verbose)
        if verbose:
            progress = ShadyBar("Downloading", max=fgoDatasets.size)
        for index in range(0, len(fgoDatasets)):
            if not fileExists(fgoDatasets[index]["LocalPath"]):
                onlineResource.downloadImage(
                    fgoDatasets[index]["AvatarURL"], fgoDatasets[index]["LocalPath"]
                )
            if verbose:
                progress.next()
        if verbose:
            progress.finish()
        log.addInfoLog("Finished to download avatar images.", verbose)

        log.addInfoLog("Update avatar descriptors...", verbose)
        fgoDatasets = datasetManager.updateDatasetsByOnlineResource(fgoDatasets)
        log.addInfoLog("Verify descriptors...", verbose)
        invalidDescriptorsCount: int = FGODatasetsService.__getInvalidDescriptorsCount(
            fgoDatasets
        )
        if invalidDescriptorsCount:
            log.addErrorLog(
                f"Failed to verify {invalidDescriptorsCount} avatar descriptors, please check log {log.getLogFilePath()} for more information.",
                verbose,
            )
        else:
            log.addInfoLog("Suceessfully verified all descriptors.", verbose)

        datasetManager.saveDatasetsToLocalFile(fgoDatasets)
        log.addInfoLog("Finish FGODatasetsService::updateFGODatasets")

    @staticmethod
    def refreshFGODatasets(verbose=True, cv2Algorithm="ORB"):
        log = LogManager()
        log.addInfoLog("Start FGODatasetsService::refreshFGODatasets")
        FGODatasetsService.__maintainAvatarDirectory()
        FGODatasetsService.__maintainDatasetDirectory()
        datasetManager = DatasetManager(log, cv2Algorithm)
        fgoDatasets = datasetManager.loadDatasetsFromLocalFile()
        log.addInfoLog("Update avatar descriptors...", verbose)
        fgoDatasets = datasetManager.updateDatasetsByOnlineResource(fgoDatasets)
        log.addInfoLog("Verify descriptors...", verbose)
        invalidDescriptorsCount: int = FGODatasetsService.__getInvalidDescriptorsCount(
            fgoDatasets
        )
        if invalidDescriptorsCount:
            log.addErrorLog(
                f"Failed to verify {invalidDescriptorsCount} avatar descriptors, please check log {log.getLogFilePath()} for more information.",
                verbose,
            )
        else:
            log.addInfoLog("Suceessfully verified all descriptors.", verbose)
        datasetManager.saveDatasetsToLocalFile(fgoDatasets)
        log.addInfoLog("Finish FGODatasetsService::refreshFGODatasets")
