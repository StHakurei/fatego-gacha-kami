from modules.gacha_record_manager import GachaRecordManager
from modules.log_manager import LogManager


class GachaRecordService:
    @staticmethod
    def checkGachaHistoryFile(verbose=True):
        log = LogManager()
        log.addInfoLog("Start GachaRecordService::checkGachaHistoryFile")
        recordManager = GachaRecordManager(log, verbose=verbose)
        recordManager.verifyGachaHistory()
        gachaHistoryErrors = recordManager.getGachaHistoryErrorDetails()
        if len(gachaHistoryErrors) > 0:
            if verbose:
                print("### Please fix following issue in your gacha history file. ###")
                for detail in gachaHistoryErrors:
                    print(f"--> {detail}")
            log.addErrorLog("Gacha history file check has failed.", verbose=verbose)
        else:
            log.addInfoLog("Gacha history file check has passed.", verbose=verbose)
        log.addInfoLog("Finish GachaRecordService::checkGachaHistoryFile")
