from modules.gacha_record_manager import GachaRecordManager
from modules.gacha_report_manager import GachaReportManager
from modules.log_manager import LogManager


class GachaReportService:
    @staticmethod
    def printBannerSummaryReport(bannerName: str):
        log = LogManager()
        log.addInfoLog("Start GachaReportService::printBannerSummaryReport")
        recordManager = GachaRecordManager(log, verbose=True)
        recordManager.verifyGachaHistory()
        if len(recordManager.getGachaHistoryErrorDetails()) > 0:
            log.addErrorLog("Gacha history file check has failed.", verbose=True)
            exit(0)

        recordManager.loadGachaHistoryFromFile()
        reportManager = GachaReportManager(log)
        reportManager.loadGachaHistory(recordManager.getGachaHistoryRecords())
        reportManager.printBannerSummaryReport(bannerName)

        log.addInfoLog("Finish GachaReportService::printBannerSummaryReport")

    @staticmethod
    def printBannerReport(bannerName: str):
        log = LogManager()
        log.addInfoLog("Start GachaReportService::printBannerReport")
        recordManager = GachaRecordManager(log, verbose=True)
        recordManager.verifyGachaHistory()
        if len(recordManager.getGachaHistoryErrorDetails()) > 0:
            log.addErrorLog("Gacha history file check has failed.", verbose=True)
            exit(0)

        recordManager.loadGachaHistoryFromFile()
        reportManager = GachaReportManager(log)
        reportManager.loadGachaHistory(recordManager.getGachaHistoryRecords())
        reportManager.printBannerRecords(bannerName)

        log.addInfoLog("Finish GachaReportService::printBannerReport")
