from modules.dataset_manager import DatasetManager
from modules.gacha_record_manager import GachaRecordManager
from modules.gacha_screenshot_operator import GachaScreenshotOperator
from modules.gacha_result_analyser import GachaResultAnalyser
from modules.log_manager import LogManager
from modules.application_exception import *
from modules.utils import *
from interfaces.opencv import *
from config.explanations import *

DEFAULT_SCREENSHOTS_DIR = "screenshots"


class FGOGachaRecognitionService:
    @staticmethod
    def recognizeGachaScreenshots(
        verbose=True,
        debug=False,
        screenshotDir=DEFAULT_SCREENSHOTS_DIR,
        bannerName="default_banner",
        featureDetectionAlgorithm="ORB",
        featureMatchingAlgorithm="FLANN",
        templateMatchingAlgorithm="TM_CCOEFF",
    ):
        log = LogManager()
        log.addInfoLog("Start FGOGachaRecognitionService::recognizeGachaScreenshots")

        if featureDetectionAlgorithm == "ORB":
            featureDetectionProvider: FeatureDetectionProvider = (
                ORBFeatureDetectionProvider()
            )
        else:
            log.addErrorLog(
                f"Unsupported feature detection algorithm {featureDetectionAlgorithm} was provided.",
                verbose=verbose,
            )
            exit(0)

        if featureMatchingAlgorithm == "FLANN":
            featureMatchingProvider: FeatureMatchingProvider = (
                BFFeatureMatchingProvider()
            )
        else:
            log.addErrorLog(
                f"Unsupported feature matching algorithm {featureMatchingAlgorithm} was provided.",
                verbose=verbose,
            )
            exit(0)

        if templateMatchingAlgorithm == "TM_CCOEFF":
            templateMatchingProvider: TemplateMatchingProvider = (
                TM_CCOEFF_TemplateMatchingProvider()
            )
        else:
            log.addErrorLog(
                f"Unsupported template detection algorithm {templateMatchingAlgorithm} was provided.",
                verbose=verbose,
            )
            exit(0)

        datasetManager = DatasetManager(log)
        fgoDatasets = datasetManager.loadDatasetsFromLocalFile()
        screenshotOperator = GachaScreenshotOperator(
            log, featureDetectionProvider, templateMatchingProvider
        )

        if not screenshotOperator.isConsistentScreenshots(DEFAULT_SCREENSHOTS_DIR):
            if verbose:
                print(f"Warning: {INCONSISTENT_NOTE}")
            log.addErrorLog(
                f"Detect non-consistent screenshots in {screenshotDir}", verbose=verbose
            )
            exit(0)

        screenshots = []
        for root, dirs, files in os.walk(screenshotDir):
            for filename in files:
                if filename != "PutYourScreenshotsInHere":
                    screenshots.append(f"{root}/{filename}")
        screenshots.sort()

        screenshotGachaResults = []
        for screenshot in screenshots:
            if verbose:
                print(f"Read and extract gacha result from {screenshot}")
            screenshotGachaResults.extend(
                screenshotOperator.extraGachaResultsFromScreenshot(screenshot, debug=debug)
            )

        resultAnalyser = GachaResultAnalyser(
            log, featureDetectionProvider, featureMatchingProvider
        )
        resultAnalyser.setDatasets(fgoDatasets)
        resultAnalyser.setBannerName(bannerName)

        if verbose:
            print("Recognizing...")
        recognizedGachaResults = resultAnalyser.batchRecognizeGachaResults(
            screenshotGachaResults
        )
        if verbose:
            print(f"Recognized {recognizedGachaResults.size} gacha results.")

        gachaRecordManager = GachaRecordManager(log, verbose)
        gachaRecordManager.verifyGachaHistory()
        gachaHistoryErrors = gachaRecordManager.getGachaHistoryErrorDetails()
        if len(gachaHistoryErrors) != 0:
            log.addErrorLog("Gacha history file check were not pass.", verbose=verbose)
            if verbose:
                for detail in gachaHistoryErrors:
                    print(detail)
            exit(0)
        else:
            log.addInfoLog("Gacha history records check were passed.", verbose=verbose)

        gachaRecordManager.loadGachaHistoryFromFile()
        gachaHistory = gachaRecordManager.getGachaHistoryRecords()
        historyRolls = 0
        if gachaHistory.size != 0:
            historyRolls = gachaHistory[gachaHistory.size - 1]["Roll"]
        for result in recognizedGachaResults:
            result["Roll"] = result["Roll"] + historyRolls

        gachaRecordManager.appendGachaHistoryRecords(recognizedGachaResults)
        gachaRecordManager.saveGachaHistoryToFile()

        log.addInfoLog("Finish FGOGachaRecognitionService::recognizeGachaScreenshots")
